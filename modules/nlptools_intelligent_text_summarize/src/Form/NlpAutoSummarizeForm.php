<?php

namespace Drupal\nlptools_intelligent_text_summarize\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NlpAutoSummarizeForm.
 */
class NlpAutoSummarizeForm extends EntityForm {


  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return \Drupal\Core\Form\ConfigFormBase
   *   Return ConfigFormBase.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entityFieldManager = $this->entityFieldManager;
    $entityManager = $this->entityManager;
    $nlp_auto_summarize = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $nlp_auto_summarize->label(),
      '#description' => $this->t("Label for the Nlp Auto Text Summarize."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $nlp_auto_summarize->id(),
      '#machine_name' => [
        'exists' => '\Drupal\nlptools_intelligent_text_summarize\Entity\NlpAutoSummarize::load',
      ],
      '#disabled' => !$nlp_auto_summarize->isNew(),
    ];

    $content_entity_types = $entityManager->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($content_entity_types as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    $entityTypeList = [];
    $bundles_target = [];

    foreach ($contentTypesList as $cont_type => $cont_val) {
      foreach ($entityFieldManager->getFieldDefinitions('node', strtolower($cont_type)) as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle())) {
          $type = $field_definition->getType();
          $bundle = $field_definition->getTargetBundle();
          if ($type == "text_with_summary") {
            $entityTypeList[$field_name] = $field_name;
            $bundles_target[$bundle] = $bundle;
          }
        }
      }
    }
    foreach ($contentTypesList as $key => $label) {
      if (!isset($bundles_target[$key])) {
        unset($contentTypesList[$key]);
      }
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#options' => $contentTypesList,
      '#title' => $this->t('Content Type'),
      '#default_value' => $nlp_auto_summarize->getContentType(),
      '#description' => $this->t("Use a content type that has Tags field."),
      '#required' => TRUE,
    ];

    $form['extract_from'] = [
      '#type' => 'select',
      '#options' => $entityTypeList,
      '#title' => $this->t('Extract from'),
      '#default_value' => $nlp_auto_summarize->getExtractFrom(),
      '#description' => $this->t("Fields list of type text_with_summary used to extract Tags from."),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Function ValidateForm.
   *
   * @param array $form
   *   Array Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Array Form Sate.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    $nodeType = strtolower($values['content_type']);
    $bundleFields = [];
    $entityFieldManager = $this->entityFieldManager;
    foreach ($entityFieldManager->getFieldDefinitions('node', $nodeType) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $bundleFields[$field_name] = $field_definition->getLabel();
      }
    }
    $allkeys = array_keys($bundleFields);
    if (!in_array($values['extract_from'], $allkeys)) {
      $form_state->setErrorByName('extract_from', $this->t('Field Extract from does not belong to the selected content type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $nlp_auto_summarize = $this->entity;
    $status = $nlp_auto_summarize->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Nlp Auto Text Summarize.', [
          '%label' => $nlp_auto_summarize->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Nlp Auto Text Summarize.', [
          '%label' => $nlp_auto_summarize->label(),
        ]));
    }
    $form_state->setRedirectUrl($nlp_auto_summarize->toUrl('collection'));
  }

}

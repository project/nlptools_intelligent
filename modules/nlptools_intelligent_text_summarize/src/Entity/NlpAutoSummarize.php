<?php

namespace Drupal\nlptools_intelligent_text_summarize\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Nlp Auto Text Summarize entity.
 *
 * @ConfigEntityType(
 *   id = "nlp_auto_summarize",
 *   label = @Translation("Nlp Auto Text Summarize"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\nlptools_intelligent_text_summarize\NlpAutoSummarizeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\nlptools_intelligent_text_summarize\Form\NlpAutoSummarizeForm",
 *       "edit" = "Drupal\nlptools_intelligent_text_summarize\Form\NlpAutoSummarizeForm",
 *       "delete" = "Drupal\nlptools_intelligent_text_summarize\Form\NlpAutoSummarizeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\nlptools_intelligent_text_summarize\NlpAutoSummarizeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "nlp_auto_summarize",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/nlptools-intelligent/nlp-auto-summarize/auto_summarize/{nlp_auto_summarize}",
 *     "add-form" = "/admin/config/nlptools-intelligent/nlp-auto-summarize/auto_summarize/add",
 *     "edit-form" = "/admin/config/nlptools-intelligent/nlp-auto-summarize/auto_summarize/{nlp_auto_summarize}/edit",
 *     "delete-form" = "/admin/config/nlptools-intelligent/nlp-auto-summarize/auto_summarize/{nlp_auto_summarize}/delete",
 *     "collection" = "/admin/config/nlptools-intelligent/nlp-auto-summarize/auto_summarize"
 *   }
 * )
 */
class NlpAutoSummarize extends ConfigEntityBase implements NlpAutoSummarizeInterface {

  /**
   * The Nlp Auto Text Summarize ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Nlp Auto Text Summarize label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Content type label.
   *
   * @var string
   */
  protected $content_type;

  /**
   * The Extract From label.
   *
   * @var string
   */
  protected $extract_from;

  /**
   * Implement getContentType() method.
   *
   * @return string
   *   Return the content type.
   */
  public function getContentType() {
    // TODO: Implement getContentType() method.
    return $this->content_type;
  }

  /**
   * Implement getExtractFrom() method.
   *
   * @return string
   *   Return the extract from.
   */
  public function getExtractFrom() {
    // TODO: Implement getExtractFrom() method.
    return $this->extract_from;
  }

}

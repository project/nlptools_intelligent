<?php

namespace Drupal\nlptools_intelligent_text_summarize\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Nlp Auto Text Summarize entities.
 */
interface NlpAutoSummarizeInterface extends ConfigEntityInterface {

  /**
   * Get te content type field.
   *
   * @return string
   *   The content type of the rule.
   */
  public function getContentType();

  /**
   * Get te extract form field.
   *
   * @return string
   *   The content type of the rule.
   */
  public function getExtractFrom();

}

<?php

namespace Drupal\nlptools_intelligent_auto_tag\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NlpAutoTagForm.
 */
class NlpAutoTagForm extends EntityForm {

  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return \Drupal\Core\Form\ConfigFormBase
   *   Return ConfigFormBase.
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entityFieldManager = $this->entityFieldManager;
    $entityManager = $this->entityManager;
    $nlp_auto_tag = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $nlp_auto_tag->label(),
      '#description' => $this->t("Label for the Nlp Auto Tag."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $nlp_auto_tag->id(),
      '#machine_name' => [
        'exists' => '\Drupal\nlptools_intelligent_auto_tag\Entity\NlpAutoTag::load',
      ],
      '#disabled' => !$nlp_auto_tag->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    $content_entity_types = $entityManager->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($content_entity_types as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    $form['content_type'] = [
      '#type' => 'select',
      '#options' => $contentTypesList,
      '#title' => $this->t('Content Type'),
      '#default_value' => $nlp_auto_tag->getContentType(),
      '#description' => $this->t("Use a content type that has Tags field."),
      '#required' => TRUE,
    ];
    $entityTypeList = [];
    $entityTargetTypeList = [];
    foreach ($contentTypesList as $cont_type => $cont_val) {
      foreach ($entityFieldManager->getFieldDefinitions('node', strtolower($cont_type)) as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle())) {
          $type = $field_definition->getType();
          $getSettings = $field_definition->getSettings();
          if ($type == "text_with_summary" || $type == "text_long" || $type == "string_long") {
            $entityTypeList[$field_name] = $field_name;
          }
          if ($type == "entity_reference" && $getSettings['target_type'] == "taxonomy_term") {
            $entityTargetTypeList[$field_name] = $field_name;
          }
        }
      }
    }
    $form['extract_from'] = [
      '#type' => 'select',
      '#options' => $entityTypeList,
      '#title' => $this->t('Extract from'),
      '#default_value' => $nlp_auto_tag->getExtractFrom(),
      '#description' => $this->t("Field used to extract Tags from."),
      '#required' => TRUE,
    ];

    $form['insert_into'] = [
      '#type' => 'select',
      '#options' => $entityTargetTypeList,
      '#title' => $this->t('Insert into'),
      '#default_value' => $nlp_auto_tag->getInsertInto(),
      '#description' => $this->t("Field to be tagged."),
      '#required' => TRUE,
    ];
    $form['number_tags'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of Tags'),
      '#maxlength' => 255,
      '#default_value' => empty($nlp_auto_tag->getNumberTags()) ? 3 : $nlp_auto_tag->getNumberTags(),
      '#description' => $this->t("Number of tags to be fetched."),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * Function ValidateForm.
   *
   * @param array $form
   *   Array Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Array Form Sate.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    $nodeType = strtolower($values['content_type']);
    $bundleFields = [];
    $entityFieldManager = $this->entityFieldManager;
    foreach ($entityFieldManager->getFieldDefinitions('node', $nodeType) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $bundleFields[$field_name] = $field_definition->getLabel();
      }
    }
    $allkeys = array_keys($bundleFields);
    if (!in_array($values['extract_from'], $allkeys)) {
      $form_state->setErrorByName('extract_from', $this->t('Field Extract from does not belong to the selected content type.'));
    }
    if (!in_array($values['insert_into'], $allkeys)) {
      $form_state->setErrorByName('insert_into',
        $this->t('Field Insert into does not belong to the selected content type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $nlp_auto_tag = $this->entity;
    $status = $nlp_auto_tag->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Nlp Auto Tag.', [
          '%label' => $nlp_auto_tag->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Nlp Auto Tag.', [
          '%label' => $nlp_auto_tag->label(),
        ]));
    }
    $form_state->setRedirectUrl($nlp_auto_tag->toUrl('collection'));
  }

}

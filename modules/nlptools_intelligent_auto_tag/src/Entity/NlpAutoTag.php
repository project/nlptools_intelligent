<?php

namespace Drupal\nlptools_intelligent_auto_tag\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Nlp Auto Tag entity.
 *
 * @ConfigEntityType(
 *   id = "nlp_auto_tag",
 *   label = @Translation("Nlp Auto Tag"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\nlptools_intelligent_auto_tag\NlpAutoTagListBuilder",
 *     "form" = {
 *       "add" = "Drupal\nlptools_intelligent_auto_tag\Form\NlpAutoTagForm",
 *       "edit" = "Drupal\nlptools_intelligent_auto_tag\Form\NlpAutoTagForm",
 *       "delete" = "Drupal\nlptools_intelligent_auto_tag\Form\NlpAutoTagDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\nlptools_intelligent_auto_tag\NlpAutoTagHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "nlp_auto_tag",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/nlptools-intelligent/nlp-auto-tag/auto_tag/{nlp_auto_tag}",
 *     "add-form" = "/admin/config/nlptools-intelligent/nlp-auto-tag/auto_tag/add",
 *     "edit-form" = "/admin/config/nlptools-intelligent/nlp-auto-tag/auto_tag/{nlp_auto_tag}/edit",
 *     "delete-form" = "/admin/config/nlptools-intelligent/nlp-auto-tag/auto_tag/{nlp_auto_tag}/delete",
 *     "collection" = "/admin/config/nlptools-intelligent/nlp-auto-tag/auto_tag"
 *   }
 * )
 */
class NlpAutoTag extends ConfigEntityBase implements NlpAutoTagInterface {

  /**
   * The Nlp Auto Tag ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Nlp Auto Tag label.
   *
   * @var string
   */
  protected $label;


  /**
   * The Content type label.
   *
   * @var string
   */
  protected $content_type;

  /**
   * The Extract From label.
   *
   * @var string
   */
  protected $extract_from;

  /**
   * The Insert into label.
   *
   * @var string
   */
  protected $insert_into;

  /**
   * The Number tags label.
   *
   * @var string
   */
  protected $number_tags;

  /**
   * Implement getContentType() method.
   *
   * @return string
   *   Return the content type.
   */
  public function getContentType() {
    // TODO: Implement getContentType() method.
    return $this->content_type;
  }

  /**
   * Implement getExtractFrom() method.
   *
   * @return string
   *   Return the extract form.
   */
  public function getExtractFrom() {
    // TODO: Implement getExtractFrom() method.
    return $this->extract_from;
  }

  /**
   * Implement getInsertInto() method.
   *
   * @return string
   *   Return the insert into value.
   */
  public function getInsertInto() {
    // TODO: Implement getInsertInto() method.
    return $this->insert_into;
  }

  /**
   * Implement getNumberTags() method.
   *
   * @return string
   *   Return the numbers of tags.
   */
  public function getNumberTags() {
    // TODO: Implement getNumberTags() method.
    return $this->number_tags;
  }

}

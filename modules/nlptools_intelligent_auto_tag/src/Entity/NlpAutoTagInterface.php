<?php

namespace Drupal\nlptools_intelligent_auto_tag\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Nlp Auto Tag entities.
 */
interface NlpAutoTagInterface extends ConfigEntityInterface {

  /**
   * Get te content type field.
   *
   * @return string
   *   The content type of the rule.
   */
  public function getContentType();

  /**
   * Get te extract form field.
   *
   * @return string
   *   The content type of the rule.
   */
  public function getExtractFrom();

  /**
   * Get te content type.
   *
   * @return string
   *   The content type of the rule.
   */
  public function getInsertInto();

  /**
   * Get te content type.
   *
   * @return string
   *   The content type of the rule.
   */
  public function getNumberTags();

}

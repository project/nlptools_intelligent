CONTENTS OF THIS FILE
---------------------

INTRODUCTION
============
Natural language processing tools (NlpTools) intelligent use of the
PHP-Science-TextRank library, which makes use of the TextRank
(automatic summarization) algorithm to summarize a text and extract
the keywords from a text.

It is made up of a central module and two submodules:

1. Nlptools_intelligent module. Main module that defines common functions.

2. Nlptools_intelligent_auto_tag module. Module that allows, based on predefined 
configuration rules for content types, to extract tags from a text and save
them in the node.

3. Nlptools_intelligent_text_summarize module. Module that allows,
based on predefined configuration rules for content types, to
extract the summary of a text and save it in the node.

REQUIREMENTS
============
This module only has a single requirement, to have installed the
PHP-Science-TextRank library.


RECOMMENDED MODULES
============
 
INSTALLATION
============
1. Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.
2. Or use composer installation: 

       composer require 'drupal/nlptools_intelligent'
       
3. Run:

       composer require 'php-science/textrank'

CONFIGURATION
============
To configure this module, do the following:

1. Go to /admin/config/nlptools-intelligent/nlp-auto-tag/auto_tag and add the
rules you think are necessary, selecting:

   1.1 the type of content on which you want to create the rule.
   
   1.2 the field of the selected content type on which to extract the keywords.
   
   1.3 the field of the selected content type on which the key words extracted
   from the previously selected field will be saved.
   
2. Go to /admin/config/nlptools-intelligent/nlp-auto-summarize/auto_summarize
and add the rules you think are necessary, selecting:

   2.1 the type of content on which you want to create the rule.
   
   2.2 the field of the selected content type on which to extract the summary.
   
3. Enjoy!!
    
TROUBLESHOOTING
============

    
 FAQ
============
 
  
MAINTENERS
============
Current maintainers:

* Maikel Maldonado del Toro (mmaldonado) - https://www.drupal.org/u/mmaldonado

* Zaidee Berges Pedrianes (zberges) - https://www.drupal.org/u/zberges
